# syntax = docker.io/docker/dockerfile:1.7-labs

ARG node=23
ARG alpine=3.21


FROM docker.io/library/node:${node}-alpine${alpine} AS web-base
RUN --mount=type=cache,target=/var/cache/apk,sharing=locked \
    --mount=type=bind,source=./package.json,target=/app/package.json \
    apk -U upgrade \
 && adduser -h /app -u 1001 -k /dev/null -D app \
 && corepack enable pnpm \
 && mkdir /app/.local \
 && chown app:app -R /app/.local \
 && pnpm -h
 # pnpm -h used to get corepack to download pnpm ahead of time, for later stages
WORKDIR /app

FROM web-base AS web-build
USER app:app
RUN --mount=type=bind,source=./package.json,target=/app/package.json \
	--mount=type=bind,source=./public,target=/app/public \
	--mount=type=bind,source=./src,target=/app/src \
	--mount=type=bind,source=./astro.config.mjs,target=/app/astro.config.mjs \
	--mount=type=bind,source=./tsconfig.json,target=/app/tsconfig.json \
	--mount=type=bind,source=./pnpm-lock.yaml,target=/app/pnpm-lock.yaml \
	--mount=type=cache,target=/app/.cache,uid=1001,gid=1001,sharing=locked \
	--mount=type=cache,target=/app/.local/share/pnpm,uid=1001,gid=1001,sharing=locked \
	--mount=type=cache,target=/app/node_modules,uid=1001,gid=1001,sharing=locked \
    pnpm install --prefer-frozen-lockfile --frozen-lockfile \
 && pnpm run build

FROM web-base AS web-deps
USER app:app
RUN --mount=type=bind,source=./package.json,target=/app/package.json \
    --mount=type=bind,source=./pnpm-lock.yaml,target=/app/pnpm-lock.yaml \
	--mount=type=cache,target=/app/.cache,uid=1001,gid=1001,sharing=locked \
	--mount=type=cache,target=/app/.local/share/pnpm,uid=1001,gid=1001,sharing=locked \
    pnpm install --prefer-frozen-lockfile --frozen-lockfile --prod

FROM web-base
ENTRYPOINT [ "node", "/app/dist/server/entry.mjs" ]

COPY --from=web-deps /app/node_modules /app/node_modules
COPY --from=web-build /app/dist /app/dist
COPY ./package.json /app/package.json
USER app:app