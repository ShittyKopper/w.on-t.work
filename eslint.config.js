import eslintPluginAstro from "eslint-plugin-astro";

export default [
  { ignores: [".astro/**/*", "dist/**/*"] },
  ...eslintPluginAstro.configs.recommended,
  ...eslintPluginAstro.configs["jsx-a11y-strict"],
  {
    rules: {
      "astro/jsx-a11y/alt-text": ["off"],
      "astro/jsx-a11y/no-redundant-roles": ["off"], // https://piccalil.li/blog/a-more-modern-css-reset/
    },
  },
];
