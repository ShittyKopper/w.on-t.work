---
layout: ../layouts/entry.astro
title: please enable your ad blocker
---

## the problem

you do not (seem to) have an ad blocker

## the fix

install an ad blocker

- [uBlock Origin for Firefox](https://addons.mozilla.org/addon/ublock-origin) <small>(including Android)</small>
- [uBlock Origin for Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) <small>(and rebrands)</small>
- [AdAway for Android](https://adaway.org)
- uhh idfk just don't get **FREE AD BLOCKER 2023 ULTIMATE NO VIRUS** or something

## false positive?

look i'm trying my best, ok. the banner is set up to show

- from a js file <small>(so blanket disabling js will make it go away)</small>
- named in a way so easylist can catch it <small>(all ad blockers worth their salt use easylist)</small>
- served from as third party a domain as i can manage
- that starts with `ads.` <small>(so any regex dns blockers will catch it)</small>
- inside a css element with the id `Ad-Container` <small>(so any css based blockers will catch it)</small>

if it still shows up in your end feel free to drop all requests to `ads.d.on-t.work`.  
just from the domain alone you can tell its not meant to be used for anything else
