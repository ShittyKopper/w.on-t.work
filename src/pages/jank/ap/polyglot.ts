import type { APIRoute } from "astro";

export const prerender = false;

export const GET: APIRoute = async ({ request }) => {
  const realType = ["Person", "Note"];
  const fakedType = new URL(request.url).searchParams.get("faked");

  const rsp = Response.json({
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      "https://w3id.org/security/v1",
      {
        "actually:attributedTo": { "@type": "@id" },
        "tryYourLuck:resolveThisOne": { "@type": "@id" },
      },
    ],
    id: request.url,
    type: fakedType == null ? realType : fakedType,
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    preferredUsername: `polyglot${fakedType == null ? "_true" : "_faked"}`,
    inbox: "https://w.on-t.work/jank/ap/inbox#notWiredUpDontActuallySendActivities",
    publicKey: {
      owner: `https://w.on-t.work/jank/ap/polyglot${fakedType == null ? "" : "?faked=Person"}`,
      publicKeyPem:
        "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl1p2lY4Cr0h3SqS6PYfL\nvOQUeBQJgWpTREInZMzLPNdcnuiH/YklcUMMaWQOCUVy5Ux7p8SuEvV+NZ0NYZKK\nxja4f0f8H01KsXUJy7mzWnSBQUlNxHzi/s+6KNviQT5bhIjnp+npWHJtIXeV42+u\nIGiChae41sPgp4Ajtl65s2+WE5axqr5ywHA+xljwbaXY9CHRBGVgd+R5SuVyuyww\ntaDrJb2SvQ+W+R4ijPMp/hqUw+mk9VuPBS1qshPBQN9p7+UMtpKwvZxyGEB2VJyb\nFc6NHV6WIWNclZtS8mhiblKRAdQl9PxhKile+uq/6yWViVhaTBvWtClst6YyyjsJ\nswIDAQAB\n-----END PUBLIC KEY-----\n",
      id: `https://w.on-t.work/jank/ap/polyglot${fakedType == null ? "" : "?faked=Person"}#placeholder-key`,
    },
    content: `<p>This is a post that owns itself. No, really, check the AP object behind it. ${
      fakedType == null
        ? ""
        : "<br><br>(Ok. I lied. This one is faked because no instance implements multiple types correctly. But then I'd argue this is an oversight in the specs in the first place and should not be supported or used by anyone ever. But who am I to know, I'm just a blob of JSON your instance hallucinates as a post and not an RDF-poisoned spec writer)"
    }</p>`,
    attributedTo: `https://w.on-t.work/jank/ap/polyglot${fakedType == null ? "" : "?faked=Person"}`,
    published: "2024-12-20T18:00:58.103Z",
    "actually:attributedTo": "https://brain.d.on-t.work/users/9lf1cyhacboh0005",
    "tryYourLuck:resolveThisOne": fakedType == null ? undefined : "https://w.on-t.work/jank/ap/polyglot",
  });

  rsp.headers.set("Cache-Control", "max-age=900,must-revalidate,stale-while-revalidate=100");
  rsp.headers.set("content-type", "application/activity+json");

  return rsp;
};
