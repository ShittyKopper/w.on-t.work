import type { APIRoute } from "astro";

export const prerender = false;

export const GET: APIRoute = async ({}) => {
  const rsp = Response.json({
    "@context": ["https://www.w3.org/ns/activitystreams"],
    id: "https://w.on-t.work/jank/ap/polyglot-reply",
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    type: "Note",
    content: `<p>the goal of this reply to see how instances handle it when they're forced to load the "true" polyglot note as a post (see inReplyTo)</p>`,
    inReplyTo: "https://w.on-t.work/jank/ap/polyglot",
    attributedTo: "https://w.on-t.work/jank/ap/polyglot",
    published: "2024-12-20T18:00:58.103Z",
    "actually:attributedTo": "https://brain.d.on-t.work/users/9lf1cyhacboh0005",
  });

  rsp.headers.set("Cache-Control", "max-age=900,must-revalidate,stale-while-revalidate=100");
  rsp.headers.set("content-type", "application/activity+json");

  return rsp;
};
