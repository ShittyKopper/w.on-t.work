import type { APIRoute } from "astro";

export const prerender = false;

export const GET: APIRoute = async ({ request }) => {
  const rsp = Response.json({
    "@context": ["https://www.w3.org/ns/activitystreams", "https://w3id.org/security/v1"],
    id: request.url,
    type: "Person",
    to: ["https://www.w3.org/ns/activitystreams#Public"],
    preferredUsername: `jank`,
    inbox: "https://w.on-t.work/jank/ap/inbox#notWiredUpDontActuallySendActivities",
    publicKey: {
      owner: `https://w.on-t.work/jank/ap/actor`,
      publicKeyPem:
        "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl1p2lY4Cr0h3SqS6PYfL\nvOQUeBQJgWpTREInZMzLPNdcnuiH/YklcUMMaWQOCUVy5Ux7p8SuEvV+NZ0NYZKK\nxja4f0f8H01KsXUJy7mzWnSBQUlNxHzi/s+6KNviQT5bhIjnp+npWHJtIXeV42+u\nIGiChae41sPgp4Ajtl65s2+WE5axqr5ywHA+xljwbaXY9CHRBGVgd+R5SuVyuyww\ntaDrJb2SvQ+W+R4ijPMp/hqUw+mk9VuPBS1qshPBQN9p7+UMtpKwvZxyGEB2VJyb\nFc6NHV6WIWNclZtS8mhiblKRAdQl9PxhKile+uq/6yWViVhaTBvWtClst6YyyjsJ\nswIDAQAB\n-----END PUBLIC KEY-----\n",
      id: `https://w.on-t.work/jank/ap/actor#placeholder-key`,
    },
  });

  rsp.headers.set("Cache-Control", "max-age=900,must-revalidate,stale-while-revalidate=100");
  rsp.headers.set("content-type", "application/activity+json");

  return rsp;
};
