import rss from "@astrojs/rss";
import type { APIContext } from "astro";
import { getCollection } from "astro:content";

export async function GET(context: APIContext) {
  return rss({
    title: "Won't Work",
    description: "kopper's corner of the web",
    site: context.site!,

    items: (await getCollection("entries"))
      .toSorted((a, b) => +b.data.published - +a.data.published)
      .map(entry => ({
        title: entry.data.title,
        pubDate: entry.data.published,
        link: `/${entry.id}/`,
        content: entry.rendered?.html,
      })),
  });
}
