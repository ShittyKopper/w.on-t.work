import type { APIRoute } from "astro";

export const prerender = false;

export const GET: APIRoute = async ({ request }) => {
  const resource = new URL(request.url).searchParams.get("resource");
  if (resource == null) {
    return new Response("not found", { status: 404 });
  }

  let actorHandle: string | null = null;
  let actorId: string | null = null;

  if (resource.includes("polyglot_faked")) {
    actorHandle = "polyglot_faked@w.on-t.work";
    actorId = `https://w.on-t.work/jank/ap/polyglot?faked=Person`;
  } else if (resource.includes("polyglot_true")) {
    actorHandle = "polyglot_true@w.on-t.work";
    actorId = `https://w.on-t.work/jank/ap/polyglot`;
  } else if (resource.includes("jank")) {
    actorHandle = "jank@w.on-t.work";
    actorId = `https://w.on-t.work/jank/ap/actor`;
  } else {
    return new Response("not found", { status: 404 });
  }

  const rsp = Response.json({
    subject: `acct:${actorHandle}`,
    links: [
      {
        rel: "self",
        type: "application/activity+json",
        href: actorId,
      },
    ],
  });

  rsp.headers.set("cache-control", "max-age=900,must-revalidate,stale-while-revalidate=100");
  rsp.headers.set("content-type", "application/jrd+json");

  return rsp;
};
