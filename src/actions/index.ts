import { ActionError, defineAction } from "astro:actions";
import { bookmarks } from "./bookmarks";

export const server = {
  bookmarks,

  test_auth: defineAction({
    handler(input, ctx) {
      if (!ctx.locals.loggedIn) throw new ActionError({ code: "UNAUTHORIZED" });

      return "ok";
    },
  }),
};
