import { ActionError, defineAction } from "astro:actions";
import { z } from "astro:schema";
import * as fs from "node:fs/promises";

interface Bookmark {
  title: string;
  url: string;
  tags: string[];
  category?: string | undefined;
  note?: string | undefined;
}

const FILE = "./data/bookmarks.json";

export const bookmarks = {
  get: defineAction({
    async handler() {
      const bookmarks: Bookmark[] = JSON.parse((await fs.readFile(FILE)).toString("utf-8"));
      return bookmarks;
    },
  }),

  add: defineAction({
    input: z.object({
      title: z.string(),
      url: z.string().url(),
      tags: z.array(z.string()).default([]),
      category: z.optional(z.string()),
      note: z.optional(z.string()),
    }),
    async handler(input, ctx) {
      if (!ctx.locals.loggedIn) throw new ActionError({ code: "UNAUTHORIZED" });

      let bookmarks: Bookmark[] = JSON.parse((await fs.readFile(FILE)).toString("utf-8"));
      bookmarks.push(input);
      await fs.writeFile(FILE, JSON.stringify(bookmarks));

      return "ok";
    },
  }),
};
