import { defineMiddleware } from "astro:middleware";
import { WEB_AUTH_TOKEN } from "astro:env/server";

export const onRequest = defineMiddleware(async (context, next) => {
  let token =
    context.cookies.get("authorization")?.value ??
    context.request.headers.get("authorization")?.split(" ", 2)[1]?.trim();

  context.locals.loggedIn = token == WEB_AUTH_TOKEN;

  const rsp = await next();

  if (context.locals.loggedIn) {
    rsp.headers.set("Cache-Control", "private");
  } else {
    rsp.headers.set("Cache-Control", "max-age=900,must-revalidate,stale-while-revalidate=100");
  }

  return rsp;
});
