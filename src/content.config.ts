import { z, defineCollection } from "astro:content";
import { glob } from "astro/loaders";

export const collections = {
  entries: defineCollection({
    loader: glob({ pattern: "**/*.{md,mdx}", base: "./src/data/entries" }),
    schema: z.object({
      title: z.string(),
      published: z.date(),
    }),
  }),

  jank: defineCollection({
    loader: glob({ pattern: "**/*.{md,mdx}", base: "./src/data/jank" }),
    schema: z.object({
      title: z.string(),
      published: z.date(),
      media: z.optional(
        z.object({
          image: z.optional(z.string()),
          video: z.optional(z.string()),
          caption: z.optional(z.string()),
        }),
      ),
    }),
  }),
};
