---
title: Outpost-FE
published: 2025-02-05
media:
  image: https://media.d.on-t.work/misskey/public-9b57c369-e0df-45f6-b4fd-aef82b42bb63.webp
  caption: >-
    A post with two media attachments (the second being cut off, as the container
    scrolls) and a bunch of hashtags rendered by the web client. The hashtags have
    been removed from the text and moved underneath the attachments, leaving behind
    whitespace which has been fixed since the screenshot was taken.
---

A web client for Mastodon-compatible instance software that I'm working on.
Currently targeting the APIs and extensions [Iceshrimp.NET] supports as that's
the software I use, but it does degrade acceptably for "vanilla" Mastodon as well.

**Extremely incomplete,** but I've been slowly chipping away at it for about a month
as of writing.

- [Try it here](https://outpost-fe.w.on-t.work)
- [Source code](https://codeberg.org/KittyShopper/mastoapi-fe)
- [Fedi thread](https://brain.d.on-t.work/notes/a32ztr4ad2f9stj6)

[Iceshrimp.NET]: https://iceshrimp.dev/iceshrimp/Iceshrimp.NET
