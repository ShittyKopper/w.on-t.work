---
title: Lua/Love2D Doom "adapter"
published: 2024-12-23
media:
  video: https://media.d.on-t.work/misskey/c515b59f-4b3b-4e65-9135-fed83735dcf4.mp4
  caption: Doom running inside Balatro
---

A version of [doomgeneric] made to be loaded as a Lua module. It was just one part
of my attempt to make a Balatro mod with a joker that ran Doom and used your level
score as the final mult, but I haven't gotten around to _that_ part just yet.

- [Find it here](https://github.com/wont-work/doomgeneric/tree/lua)
- [Fedi thread](https://brain.d.on-t.work/notes/a23n40hac3w9z8nq)
- [Bluesky thread](https://bsky.app/profile/w.on-t.work/post/3ldwdunhurc22)

[doomgeneric]: https://github.com/ozkl/doomgeneric
