---
title: ActivityPub in Minecraft
published: 2024-12-14
media:
  image: https://media.d.on-t.work/misskey/public-31e680a7-cc84-4157-8640-f6a0bec56118.webp
  caption: A fedi post by a Minecraft player named KittyShopper visible on the Iceshrimp.NET web UI. The garbled text has since been cleaned up.
---

A Paper plugin that exposes an ActivityPub instance with all the online players
as actors. Allows cross-instance (and cross-software) chat, as well as [biting]
and [holding people in your hand].

One unfortunate side effect of making this is [people assumed this was an actual
product intended to be used by people], and suggested worse protocols that are not
as fun to implement like Matrix and XMPP instead of ActivityPub (which also sucks,
but in different ways, and is way simpler to implement in a joke scale).

When will people learn that not everything has to have a "use case"?

- [Find it here](https://github.com/wont-work/mcactivitypub)
- [Fedi thread](https://brain.d.on-t.work/notes/a1rumsky1sd2m9mj)
- [Bluesky thread](https://bsky.app/profile/w.on-t.work/post/3ldcvtak6h22a)

[biting]: https://ns.mia.jetzt/as/#Bite
[holding people in your hand]: https://brain.d.on-t.work/notes/a227va50sq095g72
[people assumed this was an actual product intended to be used by people]: https://lemmy.blahaj.zone/post/19628597?scrollToComments=true
