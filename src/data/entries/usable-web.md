---
title: how to make the web usable
published: 2023-07-15
---

## use Firefox

The offers of a "Free VPN", "Free cryptoscams" or "Free AI garbage you _definitely_ want" may be tempting, but do **not** use Safari, Chrome, or any Chrome re-skins such as Edge, Opera, Vivaldi, or Brave. It simply isn't worth the headache. **Use Firefox**.

If you're on iOS then it won't make a difference as they're all Safari in the end, but then no iOS browser can use actual, proper browser extensions so you're out of luck there anyway.

If you're on Android, do note that Kiwi Browser will disable all ad blocking extensions on certain "partnered" pages with no way to turn it off. Just use Firefox.

If your favorite site yells at you to switch over to Chrome, the proper place to report that is https://webcompat.com. Afterwards try installing a user agent switcher <small>(such as [this one](https://addons.mozilla.org/en-US/firefox/addon/user-agent-string-switcher/))</small> and pretend to be Chrome. In a lot of cases they will just work.

Firefox's default configuration leaves a fair bit to be desired though, so here are some of my recommendations to make it a bit more "usable":

### Firefox configuration

Click the three bar "hamburger" menu at the top right, and open settings. In there, apply the following:

- **General**

  - [x] Use autoscrolling  
         <small>(Disabled by default on Linux because of some legacy garbage nobody but the neckbeards care about)</small>
  - [ ] Recommend extensions as you browse
  - [ ] Recommend features as you browse

- **Home**

  - [ ] Sponsored shortcuts
  - [ ] Recommended by Pocket
  - [ ] Snippets

- **Search**

  - [ ] Provide search suggestions

- **Privacy & Security**

  - Enhanced Tracking Protection

    - [x] Strict

  - Logins and Passwords

    - [ ] Ask to save logins and passwords for websites  
           <small>(And use a proper password manager such as https://bitwarden.com)</small>
    - [x] Use a Primary Password  
           <small>(If you wish to use Firefox's built-in password manager, **this is a must**)</small>

  - HTTPS-Only Mode

    - [x] Enable HTTPS-Only Mode in all windows

  - DNS over HTTPS
    - [x] Increased or Max Protection  
           <small>(Use the NextDNS provider if possible and only switch to Cloudflare if you're getting errors. If you have a Pi-Hole or other network ad blocker you can skip this step)</small>

Also, there are a few "hidden" settings that make no sense <small>(imo)</small>, so let's go tweak those as well. Open up a new tab and write `about:config` in the address bar. Click through the scary "I understand I will break everything" screen and:

Search `general.smoothScroll.msdPhysics.enabled`, click it until it says **true**  
If you're on Linux, search `middlemouse.paste`, click it until it says **false**

## use uBlock Origin

Not AdBlock, AdBlock Plus, _Best adblocker download free 2024 no virus youtube working_, or even uBlock without the Origin. Use [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/).

**Avoid installing multiple ad blockers!** This will break uBlock Origin's ability to defuse anti-adblock mechanisms, and will not "block more ads".

The only exception to this would be [AdGuard Extra](https://github.com/AdguardTeam/AdGuardExtra#firefox), which is designed to work in cooperation with uBlock Origin and can help with defusing anti-adblock mechanisms on particularly annoying websites such as Twitch, so consider installing it if you use such websites.

uBlock Origin by itself will get you 80% there, but it is capable of way more than you might expect, so here is some more configuration to get the most out of it:

### uBlock Origin configuration

Click the uBlock Origin icon in the address bar <small>(it may get hidden away under the puzzle icon)</small>, and click the three gears button in the uBO menu to open it's settings.

In the settings menu, go to the **Filter lists** tab, and change this option, just in case it's not the default already:

- [ ] Ignore generic cosmetic filters

Afterwards, click on the **xx,xxx network filters ＋ xx,xxx cosmetic filters from** heading to expand all the sections. Here, enable the following:

- **Ads**

  - [x] AdGuard – Ads (In addition to EasyList)

- **Privacy**

  - [x] AdGuard Tracking Protection

- **Annoyances**

  - [x] _everything under this section_

- **Regions, languages**
  - [x] _the relevant ones under this section_

And at the very end is **Import...** This is where the real power of uBO lies. Toggle the import box and throw the following on there:

(Make sure there is only one address per line)

```
https://github.com/DandelionSprout/adfilt/raw/master/AnnoyancesList
https://github.com/yokoffing/filterlists/raw/main/annoyance_list.txt
https://github.com/LanikSJ/webannoyances/raw/master/ultralist.txt
https://github.com/piperun/iploggerfilter/raw/master/filterlist
```

Depending on the sites you frequent, these may also be worth adding:

**Twitch**

```
https://github.com/DandelionSprout/adfilt/raw/master/AntiAmazonListForTwitch.txt
https://github.com/DandelionSprout/adfilt/raw/master/TwitchPureViewingExperience.txt
```

**Imgur**

```
https://github.com/DandelionSprout/adfilt/raw/master/ImgurPureImageryExperience.txt
```

**Fandom** / **Wikia**

```
https://github.com/DandelionSprout/adfilt/raw/master/WikiaPureBrowsingExperience.txt
```

**Twitter**

```
https://github.com/DandelionSprout/adfilt/raw/master/TwitterPureReadingExperience.txt
```

**YouTube**  
<small>(This one removes a lot of functionality some might want to keep)</small>

```
https://github.com/DandelionSprout/adfilt/raw/master/YouTubeEvenMorePureVideoExperience.txt
```

You can also convert embeds and whatnot into "click-to-load" placeholders by adding this:

```
https://github.com/yokoffing/filterlists/raw/main/click2load.txt
```

After adding these URLs, click **Apply changes** and **Update now** on the top bar, and wait for everything to load in.

## other useful add-ons

While uBO configured for maximum annoyance blocking will serve you well, there are a few other add-ons that may enhance the web in other ways. Unfortunately most of these are desktop only without jumping through major hoops.

While mostly redundant with the uBO configuration, [Consent-O-Matic](https://addons.mozilla.org/en-US/firefox/addon/consent-o-matic/) can automatically deal with GDPR consent prompts on your behalf.

[uBlacklist](https://addons.mozilla.org/en-US/firefox/addon/ublacklist) will allow you to hide shit from search results. While it does have a concept of filter lists like uBO, there aren't any major filter lists so you will have to individually block annoying websites one by one.

If you're a heavy YouTube watcher, consider getting [SponsorBlock](https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/) and [DeArrow](https://addons.mozilla.org/en-US/firefox/addon/dearrow/). [BlockTube](https://addons.mozilla.org/en-US/firefox/addon/blocktube/) is also useful if there are some channels that simply won't go away no matter how much you click "not interested".

[Indie Wiki Buddy](https://addons.mozilla.org/en-US/firefox/addon/indie-wiki-buddy/) and [Redirect to wiki.gg](https://addons.mozilla.org/en-US/firefox/addon/redirect-to-wiki-gg/) are useful to find wikis that have left the Fandom dumpster behind.
