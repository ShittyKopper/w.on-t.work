---
title: sidenotes
published: 2024-12-31
---

the footnote system i used to use on this here site was your ol' reliable github flavored markdown footnotes, which are, lets face it, extremely boring and [^unwieldy]

[^unwieldy]: you have to jump _aaaallll_ the way down to the bottom to read them, and come _aaaallll_ the way back up

since the very beginning i wanted to put them on the side instead. that's why all this text has been left aligned instead of being [^centered]

[^centered]: and not just because centering in css is a punchline

of course, if you're are were <small>[sic]</small> on a phone, you may not have that space. in that case the sidenotes should degrade nicely, but also not obstruct the article where unwanted, [^so_having_them_be_toggleable_was_a_must]

[^so_having_them_be_toggleable_was_a_must]:
    the fact that it still works like that on desktop is because i just liked it that way.

    it also helps distinguish which sidenote maps to which part of text without needing to clutter up things, and lets me write longer ones without needing to worry about space

## god i fucking hate the w3c

my first attempt involved simply putting a `<details>` element. you know. semantic html. browser takes care of all the heavy lifting. what's not to love? nope. won't work. doesn't go inside paragraphs. why? fuck you thats why

i personally blame it on the w3c, because they created the initial html spec, and i already have a hatred for w3c for bringing activitypub into the world. how can a single organization cause such widespread agony?

anyway, using anchors and a bit of CSS still resulted in a JS-free but [^an_unimaginably_clunkier] implementation, but i have no idea how the accessibility holds up.

[^an_unimaginably_clunkier]: the page jumps every time you open a sidenote, which is _not jarring at all_

to lessen the jank, i try to use JS where available to manually implement the open/close behavior, but that's just gonna create it's own jank for the power users and screen readers. you can't win.

## todo: not responsive enough

because i'm using [^ancient_css] this is not [^as_fluid_as_i_would've_liked], but i haven't been able to find any way to handle this with our lords and saviours flex and/or grid, so here it stays.

[^ancient_css]: `float`? seriously?

[^as_fluid_as_i_would've_liked]: look at the text when resizing the page

## update: what if a div was a paragraph

after [a suggestion] in bluesky, i reconfigured [^remark-rehype] to emit `<div role=paragraph>` instead. in theory it should have the same accessibility properties as a `<p>` but without the restrictions on content.

[^remark-rehype]: the markdown to html translator used in Astro

while i was there, i tried to make the markdown footnote syntax emit details/summary, and to update my CSS to style _that_ instead of my custom hacky components, but unfortunately i do not know the arcane CSS magic to make it work correctly outside _Webkit_, of all engines.

maybe i'll switch back to the custom component, maybe i'll just deal with it being broken when not at the end of a line. i do not know.

[a suggestion]: https://bsky.app/profile/did:web:kopper.d.on-t.work/post/3lemm6gs2xk2g
