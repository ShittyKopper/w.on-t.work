---
title: make your fedi software small instance friendly with this 1 shocking trick
published: 2024-11-07
---

[The `as:replies` collection](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-replies). Expose the `as:replies` collection.

The `as:replies` collection solves one of the most annoying problems of small instances: Missing replies. It's surprising how many ActivityPub software straight up don't bother with the replies collection when it's so simple to expose to other instances (albeit _making use of it_ is certainly harder)

Only 1 of the top 9 most used ActivityPub software (according to [FediDB](https://fedidb.org/)'s _Software User Distribution_ graph) expose it, and it's _Mastodon_.

How much work is it? Not much, here:

1. Expose an endpoint returning an `as:Collection` or `as:OrderedCollection` where the `as:items` contains the direct replies to your object.
2. Put the endpoint to that collection in the `replies` property of your object.
3. Throw a post of yours to https://browser.pub and see if it pulls in the replies correctly. ([Like this](https://browser.pub/https://brain.d.on-t.work/notes/a06ku16iazg0dqt5))
4. Profit.

Simply by doing this, you now give instances the ability to load replies they may not have seen, without resorting to [hacks that work outside the protocol](https://github.com/nanos/FediFetcher). You also get access control to determine which replies can be seen (on the instance level, anyway. Which is more than the hacks can give you)

## Who benefits from this?

Not enough people, sadly. To my knowledge the only software that make use of this collection are:

- Mastodon, in an extremely limited capacity (replies posted in the same instance as the parent post only), though [this may change soon](https://github.com/mastodon/mastodon/pull/32615)
- GoToSocial
- Iceshrimp's .NET rewrite
- Wafrn
- Akkoma (Maybe? There's code that seems to fetch the collection, but I can't read Elixir to save my life)
- ...that's it...? I think? There may be other smaller software I haven't heard of that benefit from it, but the point I'm trying to express is how none of the "big names" beyond Mastodon are on here, and even that uses it in a limited way.

Now, I don't blame other software for not backfilling the collection. It's _extremely easy_ to shoot yourself in the foot and create a forkbomb due to the recursive nature of it (I've certainly done it [several](https://iceshrimp.dev/iceshrimp/Iceshrimp.NET/pulls/36) [times](https://iceshrimp.dev/iceshrimp/Iceshrimp.NET/pulls/51) when implementing it on Iceshrimp's rewrite), and there are valid concerns regarding resource usage and efficiency, how ActivityPub doesn't have any facilities for batch requests, [^the_wasteful_amount_of_requests_needed_to_make_this_happen], and the amount of database space you waste to storing URLs that should've been predefined relative to the object ID itself.

[^the_wasteful_amount_of_requests_needed_to_make_this_happen]: One funny thing about the efficiency concerns around AP is how, according to my logs, anyway, it's _only_ AodeRelay, Iceshrimp.NET, and GoToSocial, that attempt even basic things like HTTP/2, which in many cases is just a switch you need to flip in your HTTP client. But I digress...

## An easier, more efficient way?

There is _one_ solution in the horizon that may allow for more efficient backfill. It's not going to solve everything, a lot of the problems here are more or less fundamental to how ActivityPub has been designed.

However, at least the unnecessary recursion can be solved with the use of [`as:context`](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-context) to represent a conversation. There is an even smaller number of instance software that do this (as far as I'm aware it's just the streams repository, and NodeBB, with the Pleroma family having a partial implementation that doesn't help with backfilling).

It's still not as well defined. Actually, it's not defined _at all_. [FEP-7888](https://codeberg.org/fediverse/fep/src/branch/main/fep/7888/fep-7888.md) is the closest thing to a specification, and even that's vague as all hell. This, combined with the existence of [FEP-76ea](https://codeberg.org/fediverse/fep/src/branch/main/fep/76ea/fep-76ea.md) throwing a new and frankly unnecessary spanner in the works in getting one standard way defined and adopted, makes me a bit hesitant on telling people to implement it _yet_, but when the FEP crew actually starts working on something instead of sitting on their hands debating [^minute_semantic_differences_and_annoying_everyone_involved_in_the_process], we'll maybe get to have a single collection we can enumerate to load an entire thread from start to finish, as efficiently as ActivityPub can do while being ActivityPub.

[^minute_semantic_differences_and_annoying_everyone_involved_in_the_process]: _Nobody_ actually keeps the data from remote activities why do you expect people to put activities instead of objects in the collection? SSDs for databases don't grow on trees!

Until then, the replies collection is the best we have. Just like with the rest of ActivityPub, it's Mediocre Enough™ and What Mastodon Does Anyway™. So you have no excuse not to.

PS: if you're interested in making use of other instances' replies collections to backfill, feel free to take a look at [the implementation in Iceshrimp.NET](https://iceshrimp.dev/iceshrimp/Iceshrimp.NET/src/branch/dev/Iceshrimp.Backend/Core/Queues/BackfillQueue.cs), which is mostly self contained in that one file. That said, don't port over the code verbatim, I'm sure there are a few issues lurking in there I haven't yet seen.
