#!/bin/sh
set -eu

cd /home/kopper/web3
buildctl --addr=podman-container://buildkitd build \
  --frontend dockerfile.v0 \
  --local context=. \
  --local dockerfile=. \
  --export-cache type=inline \
  --output type=docker,name=localhost/kopper/web3 | podman load

podman-compose down || true
podman-compose up -d
podman restart caddy # caddy cache-handler sucks so this is the best way i could find to wipe the cache without exposing an admin api to the entire world
