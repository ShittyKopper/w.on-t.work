// @ts-check
import { defineConfig, envField } from "astro/config";
import node from "@astrojs/node";
import mdx from "@astrojs/mdx";
import icon from "astro-icon";
import rehypeWidont from "rehype-widont";

const prod = process.env.NODE_ENV === "production";

// https://astro.build/config
export default defineConfig({
  site: "https://w.on-t.work",
  output: "static",
  compressHTML: prod,

  env: {
    schema: {
      WEB_MINIFLUX_TOKEN: envField.string({
        context: "server",
        access: "secret",
        optional: true,
      }),

      WEB_AUTH_TOKEN: envField.string({
        context: "server",
        access: "secret",
        optional: true,
      }),
    },
  },

  vite: {
    build: {
      assetsInlineLimit: 0,
    },
  },

  adapter: node({
    mode: "standalone",
  }),

  markdown: {
    rehypePlugins: [rehypeWidont],
    remarkRehype: {
      handlers: {
        paragraph(state, node) {
          /** @type {Element} */
          const result = {
            type: "element",
            tagName: "div",
            properties: {
              role: "paragraph",
            },
            children: state.all(node),
          };
          state.patch(node, result);
          return state.applyData(node, result);
        },

        footnoteReference(state, node) {
          const id = String(node.identifier).toUpperCase();
          const footnote = state.footnoteById.get(id);

          /** @type {Element} */
          const summary = {
            type: "element",
            tagName: "summary",
            properties: {},
            children: [{ type: "text", value: footnote?.label?.replaceAll("_", " ") }],
          };
          state.patch(node, summary);

          /** @type {Element} */
          const content = {
            type: "element",
            tagName: "div",
            properties: {
              className: "flow",
            },
            children: state.all(footnote),
          };
          state.patch(node, content);

          /** @type {Element} */
          const details = {
            type: "element",
            tagName: "details",
            properties: {
              className: "note",
              name: "sidenote",
            },
            children: [summary, content],
          };
          state.patch(node, details);

          return state.applyData(node, details);
        },
      },
    },
  },

  integrations: [
    mdx({
      optimize: true,
    }),
    icon({
      include: {
        "simple-icons": ["activitypub", "github", "codeberg", "buymeacoffee", "bluesky"],
      },
      svgoOptions: {
        multipass: prod,
      },
    }),
  ],
});
